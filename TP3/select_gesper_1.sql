SELECT dateDeb, dateFin, type
FROM formation;

SELECT nomservice
FROM service;

SELECT nom, prenom, noserv
FROM salaries;

SELECT nom, prenom, noserv
FROM salaries
WHERE noserv = 2;

SELECT nom, prenom
FROM salaries
WHERE adresse = 'Rennes'
AND rythme = 'Normal';

SELECT nom, prenom
FROM salaries
MINUS
SELECT nom, prenom
FROM salaries
WHERE adresse = 'Rennes'
AND rythme = 'Normal';

SELECT nom, prenom
FROM salaries
WHERE noss LIKE '2%';

SELECT nom, prenom
FROM salaries
WHERE adresse = 'Betton' or adresse = 'Rennes'
AND contrat != 'CDD';

SELECT DISTINCT contrat
FROM salaries;

SELECT matricule
FROM salaries
WHERE anciennete >= 5
AND anciennete <= 10;

SELECT nom
FROM salaries
ORDER BY nom;