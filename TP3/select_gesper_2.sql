SELECT nom, prenom, nomservice
FROM salaries, service
WHERE service.noserv = salaries.noserv;

SELECT type, matricule
FROM suivre, formation
WHERE suivre.codeform = formation.codeform;

SELECT nom, designarret, datedeb, datefin
FROM salaries, arrettravail, typearret
WHERE arrettravail.matricule = salaries.matricule
AND arrettravail.codetypearr = typearret.codetypearr;

SELECT type, nom
FROM suivre, salaries, formation
WHERE suivre.codeform = formation.codeform
AND suivre.matricule = salaries.matricule;

SELECT nom
FROM arrettravail, salaries
WHERE arrettravail.matricule = salaries.matricule
AND datefin IS NULL;

SELECT nom, prenom, salaire
FROM salaries, grille
WHERE salaries.codecateg = grille.codecateg
AND salaries.codecoeff = grille.codecoeff;

SELECT nom, designarret, datedeb, datefin
FROM salaries, arrettravail, typearret
WHERE arrettravail.matricule = salaries.matricule
AND arrettravail.codetypearr = typearret.codetypearr
AND designarret = 'Accident'
AND contrat = 'CDD';

SELECT DISTINCT nom, prenom
FROM suivre, salaries
WHERE suivre.matricule = salaries.matricule
AND anciennete >= 5;
-- 2
SELECT DISTINCT s1.nom, s2.nom
FROM salaries s1, salaries s2
WHERE s1.nom != s2.nom
AND s1.noserv = 2
AND s2.noserv = 2;

SELECT nom, prenom
FROM salaries
WHERE NOT EXISTS (SELECT matricule
                  FROM service
                  WHERE service.Matriculeresp = salaries.matricule
                  );
-- 3
SELECT matricule
FROM salaries
MINUS
SELECT matriculeresp
FROM service;

SELECT nom, adresse
FROM salaries
WHERE matricule IN (
    SELECT matricule
    FROM salaries
    MINUS
    SELECT matricule
    FROM arrettravail
    );

SELECT nom
FROM salaries, service
WHERE salaries.noserv = service.noserv
AND service.nomservice = 'Commercial'
UNION
SELECT nom
FROM salaries, service
WHERE salaries.noserv = service.noserv
AND service.nomservice = 'Personnel';

SELECT nom, prenom
FROM salaries
WHERE matricule IN (
    (SELECT matricule
    FROM salaries
    MINUS
    SELECT matricule
    FROM arrettravail)
    MINUS
    SELECT matricule
    FROM formation
    );

SELECT DISTINCT type
FROM (SELECT nom, type
      FROM formation, suivre, salaries
      WHERE salaries.matricule = suivre.matricule
      AND formation.codeform = suivre.codeform
      )
WHERE nom = 'Le Gall'
INTERSECT
SELECT DISTINCT type
FROM (SELECT nom, type
      FROM formation, suivre, salaries
      WHERE salaries.matricule = suivre.matricule
      AND formation.codeform = suivre.codeform
      )
WHERE nom = 'Tual';
-- 4
SELECT COUNT(*) as nbsalaries
FROM salaries;

SELECT AVG(anciennete) as moyenneanciennete
FROM salaries
WHERE contrat = 'CDD';

SELECT MIN(anciennete) as minanciennete, MAX(anciennete) as maxanciennete
FROM salaries
WHERE contrat != 'CDD';

SELECT COUNT(*) as nbarret
FROM arrettravail
WHERE datedeb > to_timestamp('2017-01-01','YYYY-MM-DD');

SELECT SUM(nbpararret) as nbTotal
FROM (SELECT (datefin - datedeb) as nbpararret
      FROM arrettravail
      );

    