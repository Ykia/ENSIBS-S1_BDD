-- 1
SELECT COUNT(*) as nbsalaries, salaries.noserv
FROM salaries
GROUP BY salaries.noserv;

SELECT AVG(anciennete) as moyanciennete, contrat
FROM salaries
GROUP BY contrat;

SELECT COUNT(*) as nbsalaries, adresse
FROM salaries
WHERE contrat = 'CDI'
GROUP BY adresse;

SELECT COUNT(*) as nbsalaries, nomservice
FROM salaries, service
WHERE salaries.noserv = service.noserv
GROUP BY nomservice;

SELECT COUNT(*), formation.datedeb, formation.type
FROM formation, suivre
WHERE suivre.codeform = formation.codeform
GROUP BY formation.datedeb, formation.type;

-- 2
SELECT nom
FROM salaries
WHERE noserv IN (SELECT noserv
                 FROM service
                 WHERE nomservice = 'Personnel'
                 );

SELECT nom, prenom, matricule
FROM salaries
WHERE matricule IN (SELECT DISTINCT matricule
                    FROM suivre
                    );

SELECT nom, prenom, matricule
FROM salaries
WHERE matricule IN (SELECT DISTINCT matricule
                    FROM suivre
                    WHERE codeform IN (SELECT codeform
                                       FROM formation
                                       WHERE type = 'Raft'
                                       )
                    );

SELECT nom, prenom, matricule, anciennete
FROM salaries
WHERE anciennete > (SELECT AVG(anciennete)
                    FROM salaries
                    );

-- 3
SELECT nomservice
FROM service
WHERE noserv IN (SELECT salaries.noserv
                  FROM salaries
                  GROUP BY salaries.noserv
                  HAVING COUNT(*) > 2
                 );

SELECT codeform
FROM formation 
WHERE formation.codeform IN (SELECT suivre.codeform as numform
                 FROM suivre
                 GROUP BY suivre.codeform
                 HAVING COUNT(*)  > 3
                 );

SELECT codeform
FROM suivre
GROUP BY suivre.codeform
HAVING COUNT(*) = (SELECT MAX(COUNT(*))
                   FROM suivre
                   GROUP BY suivre.codeform
                   );

SELECT typearret.designarret
FROM arrettravail, typearret
WHERE arrettravail.codetypearr = typearret.codetypearr
GROUP BY typearret.designarret
HAVING COUNT(*) = (SELECT MAX(COUNT(*))
                   FROM arrettravail, typearret
                   WHERE arrettravail.codetypearr = typearret.codetypearr
                   GROUP BY typearret.designarret
                   );
                
SELECT nom, prenom
FROM salaries
WHERE matricule IN (SELECT matricule
                    FROM (SELECT (datefin - datedeb) as temps, matricule
                          FROM formation, suivre
                          WHERE formation.codeform = suivre.codeform
                          )
                    GROUP BY matricule
                    HAVING SUM(temps) > 5
                    );

    
                
