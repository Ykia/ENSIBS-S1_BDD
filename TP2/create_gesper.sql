CREATE TABLE salaries(
   matricule INT,
   nom VARCHAR(40),
   prenom VARCHAR(40),
   adresse VARCHAR(40),
   noss VARCHAR(40),
   datenais DATE,
   anciennete INT,
   contrat VARCHAR(40),
   creditconge INT,
   rythme VARCHAR(40),
   codecateg INT,
   codecoeff INT,
   noserv INT,
   PRIMARY KEY(matricule)
);
INSERT INTO salaries VALUES (1,	'Dibou','Olivier','St Pierre',		'1700464689341',to_timestamp('1990-04-28','YYYY-MM-DD'),11,NULL, 15,'2.8'	 ,1,200,1);
INSERT INTO salaries VALUES (2,	'Bliet','Véronique','Nantes',		'2720228102852',to_timestamp('1992-02-22','YYYY-MM-DD'),6, NULL, 40,'3.8'	 ,1,400,3);
INSERT INTO salaries VALUES (3,	'Esnault','Jean-Marc','Rennes',		'1711035200121',to_timestamp('1991-10-13','YYYY-MM-DD'),2, 'CDD',21,'Normal',2,300,5);
INSERT INTO salaries VALUES (4,	'Derouet','Sandrine','Saint-germain',	'2720949210320',to_timestamp('1992-09-01','YYYY-MM-DD'),1, 'CDI',10,'Normal',4,400,1);
INSERT INTO salaries VALUES (5,	'Le Gall','Franck','Ploumagoar',	'1711022023045',to_timestamp('1991-10-07','YYYY-MM-DD'),2, 'CDD',40,'2.8'	 ,3,200,2);
INSERT INTO salaries VALUES (6,	'Tual','Sophie','Rennes',		'2711022140152',to_timestamp('1991-10-22','YYYY-MM-DD'),1, 'CDI',23,'Normal',4,350,4);
INSERT INTO salaries VALUES (7,	'Rams','Karine','Rennes',		'2730629320210',to_timestamp('1993-06-11','YYYY-MM-DD'),5, 'CDI',10,'Normal',1,400,2);
INSERT INTO salaries VALUES (8,	'Pilet','Catherine','Rennes',		'2721114020035',to_timestamp('1992-11-20','YYYY-MM-DD'),5, 'CDI',2, '2.8'	 ,2,300,4);
INSERT INTO salaries VALUES (9,	'Menard','Marie','Rennes',		'2730522400200',to_timestamp('1993-05-10','YYYY-MM-DD'),2, 'CDD',12,'2.8'	 ,2,300,3);
INSERT INTO salaries VALUES (10,'Vuillermoz','Claire','Betton',		'2641035123452',to_timestamp('1984-10-22','YYYY-MM-DD'),15,'CDI',23,'Normal',1,400,1);


CREATE TABLE typearret(
   codetypearr INT,
   designarret VARCHAR(40),
   PRIMARY KEY(codetypearr)
);
INSERT INTO typearret VALUES (1,'Maladie');
INSERT INTO typearret VALUES (2,'Accident');
INSERT INTO typearret VALUES (3,'Thérapeutique');


CREATE TABLE service(
   noserv INT,
   nomservice VARCHAR(40),
   objectifs VARCHAR(40),
   Matriculeresp INT,
   PRIMARY KEY(noserv)
);
INSERT INTO service VALUES (1,'Personnel',NULL,1);
INSERT INTO service VALUES (2,'Informatique',NULL,5);
INSERT INTO service VALUES (3,'Comptabilité',NULL,2);
INSERT INTO service VALUES (4,'Commercial',NULL,6);
INSERT INTO service VALUES (5,'Technique',NULL,3);


CREATE TABLE arrettravail(
   noarret INT,
   datedeb DATE,
   datefin DATE,
   codetypearr INT,
   matricule INT,
   PRIMARY KEY(noarret)
);
INSERT INTO arrettravail VALUES (5,to_timestamp('2016-10-17','YYYY-MM-DD'),to_timestamp('2016-11-18','YYYY-MM-DD'),1,3);
INSERT INTO arrettravail VALUES (6,to_timestamp('2016-11-20','YYYY-MM-DD'),NULL,2,3);
INSERT INTO arrettravail VALUES (7,to_timestamp('2016-12-12','YYYY-MM-DD'),to_timestamp('2016-12-15','YYYY-MM-DD'),1,4);
INSERT INTO arrettravail VALUES (8,to_timestamp('2017-01-03','YYYY-MM-DD'),to_timestamp('2017-01-10','YYYY-MM-DD'),2,6);
INSERT INTO arrettravail VALUES (9,to_timestamp('2017-02-02','YYYY-MM-DD'),NULL,3,4);
INSERT INTO arrettravail VALUES (10,to_timestamp('2017-03-03','YYYY-MM-DD'),to_timestamp('2017-03-07','YYYY-MM-DD'),2,9);
INSERT INTO arrettravail VALUES (11,to_timestamp('2017-03-03','YYYY-MM-DD'),to_timestamp('2017-04-01','YYYY-MM-DD'),1,7);
INSERT INTO arrettravail VALUES (12,to_timestamp('2017-03-05','YYYY-MM-DD'),to_timestamp('2017-04-12','YYYY-MM-DD'),2,5);
INSERT INTO arrettravail VALUES (13,to_timestamp('2017-04-05','YYYY-MM-DD'),to_timestamp('2017-04-15','YYYY-MM-DD'),3,2);
INSERT INTO arrettravail VALUES (14,to_timestamp('2017-04-05','YYYY-MM-DD'),to_timestamp('2017-04-20','YYYY-MM-DD'),2,5);
INSERT INTO arrettravail VALUES (15,to_timestamp('2017-04-06','YYYY-MM-DD'),to_timestamp('2017-04-10','YYYY-MM-DD'),3,8);


CREATE TABLE formation(
   codeform INT,
   datedeb DATE,
   datefin DATE,
   type VARCHAR(40),
   PRIMARY KEY(codeform)
);
INSERT INTO formation VALUES (1,to_timestamp('2016-04-17','YYYY-MM-DD'),to_timestamp('2016-04-19','YYYY-MM-DD'),'Gym');
INSERT INTO formation VALUES (2,to_timestamp('2016-11-24','YYYY-MM-DD'),to_timestamp('2016-11-30','YYYY-MM-DD'),'Raft');
INSERT INTO formation VALUES (3,to_timestamp('2016-05-20','YYYY-MM-DD'),to_timestamp('2016-05-30','YYYY-MM-DD'),'Parapent');
INSERT INTO formation VALUES (4,to_timestamp('2016-06-01','YYYY-MM-DD'),to_timestamp('2016-06-05','YYYY-MM-DD'),'Boxe');


CREATE TABLE suivre(
   matricule INT,
   codeform INT
);
INSERT INTO suivre VALUES (1,1);
INSERT INTO suivre VALUES (1,2);
INSERT INTO suivre VALUES (3,2);
INSERT INTO suivre VALUES (3,3);
INSERT INTO suivre VALUES (3,4);
INSERT INTO suivre VALUES (5,2);
INSERT INTO suivre VALUES (6,2);
INSERT INTO suivre VALUES (6,3);
INSERT INTO suivre VALUES (8,1);
INSERT INTO suivre VALUES (9,1);
INSERT INTO suivre VALUES (10,4);


CREATE TABLE coefficient(
   codecoeff INT
);
INSERT INTO coefficient VALUES (100);
INSERT INTO coefficient VALUES (150);
INSERT INTO coefficient VALUES (200);
INSERT INTO coefficient VALUES (250);
INSERT INTO coefficient VALUES (300);
INSERT INTO coefficient VALUES (350);
INSERT INTO coefficient VALUES (400);
INSERT INTO coefficient VALUES (450);
INSERT INTO coefficient VALUES (500);


CREATE TABLE grille(
   codecateg INT,
   codecoeff INT,
   salaire INT
);
INSERT INTO grille VALUES (1,400,5500);
INSERT INTO grille VALUES (1,500,6500);
INSERT INTO grille VALUES (2,300,4000);
INSERT INTO grille VALUES (2,400,4500);
INSERT INTO grille VALUES (3,200,2500);
INSERT INTO grille VALUES (3,250,3500);
INSERT INTO grille VALUES (4,150,1500);
INSERT INTO grille VALUES (4,200,2000);
INSERT INTO grille VALUES (4,350,2500);
INSERT INTO grille VALUES (4,400,3000);


CREATE TABLE categorie(
   codecateg INT,
   designcateg VARCHAR(40)
);
INSERT INTO categorie VALUES (1,'Employé');
INSERT INTO categorie VALUES (2,'Employé');
INSERT INTO categorie VALUES (3,'Ouvrier');
INSERT INTO categorie VALUES (4,'Etam');

